# Лабораторная работа 2

**Название:** "Разработка драйверов блочных устройств"

**Цель работы:** Получить знания и навыки разработки драйверов блочных устройств для OS Linux.

## Описание функциональности драйвера

Один первичный раздел размером 10 Мбайт и один расширенный раздел,
содержащий два логических раздела размером 20 Мбайт каждый.

## Инструкция по сборке

`make`

## Инструкция пользователя

`sudo insmod main.ko`

## Примеры использования

### Смотрим устройство
`lsblk -l /dev/mydisk` \
![img.png](img.png)

### Смотрим разделы устройства
`sudo fdisk -l /dev/mydisk` \
![img_1.png](img_1.png)

### Форматируем и монтируем диск
`sudo mkfs.vfat -F16 /dev/mydisk5` \
`sudo mkdir -p /mnt/mydisk5` \
`sudo mount /dev/mydisk5 /mnt/mydisk5` \
![img_2.png](img_2.png)

То же самое проделываем и с `mydisk6`

### Убираем монтирование
`sudo umount /mnt/mydisk5`

### Перформанс
С виртуального на виртуальное \
![img_3.png](img_3.png)

Из реального в виртуальное \
![img_4.png](img_4.png)

## Вывод
~~Борис, напиши вывод~~ \
В ходе выполнения данной лабораторной работы мы научились писать драйвер блочного устройства из заготовки и также умело делегировать задачи своему коллеге 
